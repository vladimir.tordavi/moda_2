import React from 'react';
import './App.css';
import Header from './layout/Header';
import Body from './layout/Body';
import Footer from './layout/Footer';
import {
  BrowserRouter as Router,
} from "react-router-dom";

function App() {
  return (
    <div>
      <Router>
        <Header/>
        <Body/>
        <Footer/>
      </Router>
    </div>
  );
}

export default App;
