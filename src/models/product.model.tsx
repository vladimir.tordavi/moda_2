interface IProductModel {
    id: string;
    title: string;
    description: string;
    material: string;
    brand: string;
    size: string;
    colors: string[];
    inventary: number;
    price: number;
    category: string;
    isOnCart: boolean;
    isOnWishes: boolean;
    image: string,
    galery?: string[]
}

export default IProductModel;