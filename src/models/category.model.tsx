
interface ICategoryModel {
    id: string;
    title: string;
    icon: string;
}

export default ICategoryModel;