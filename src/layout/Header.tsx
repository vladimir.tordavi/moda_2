import React from 'react';
import { Segment, Grid, Header as H, Image, Icon } from 'semantic-ui-react';
import Cart from './components/store/components/Cart';
import "../css/header.css";
import logo from '../logo.svg';
import { useNavigate } from 'react-router-dom';
import Wish from './components/store/components/Wish';

const Header = () => {

    const navigate = useNavigate()

    return(
        <div className="header-container">
            <Grid>
                <Grid.Row columns={3}>
                    <Grid.Column computer={2} mobile={4} onClick={() => navigate("/")}> 
                        <img src={logo} className="App-logo pointer" alt="logo" />
                    </Grid.Column>
                    <Grid.Column computer={3} mobile={4} onClick={() => navigate("/")}> 
                        <H className='header-title pointer' size='huge'>
                            <H.Content>MODA 2.0</H.Content>
                        </H>
                    </Grid.Column>
                    <Grid.Column computer={8} mobile={4}></Grid.Column>
                    <Grid.Column computer={1}>
                        <Wish/>
                    </Grid.Column>
                    <Grid.Column computer={1}>
                        <Cart/>
                    </Grid.Column>
                    <Grid.Column computer={1}>
                        <Icon 
                            className='nav-item pointer' 
                            name='user' 
                            size='large'
                            onClick={() => navigate("/login")}
                        />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </div>
    );
}

export default Header;