import React, { useState, useEffect } from 'react';
import { Grid, Header, Input, Segment, Image, Button } from 'semantic-ui-react';
import IProductModel from '../../../models/product.model';
import products from '../../../data/products.data';
import "../../../css/payment.css";
import { useNavigate } from 'react-router-dom';

interface IState {
    productList: IProductModel[];
    name: string;
    nameValid: boolean;
    street: string;
    streetValido: boolean;
    numExt: string;
    numExtValido: boolean;
    numInt: string;
    zip: string;
    zipValido: boolean;
    col: string;
    colValido: boolean;
    city: string;
    cityValido: boolean;
    state: string;
    stateValido: boolean;
    card: string;
    cardValido: boolean;
    dateExp: string;
    dateExpValido: boolean;
    ccv: string;
    ccvValido: boolean;
    totalAccount: number;
}

const Payment = () => {

    const [state, setState] = useState<IState>({
        productList: [],
        name: "",
        nameValid: true,
        street: "",
        streetValido: true,
        numExt: "",
        numExtValido: true,
        numInt: "",
        zip: "",
        zipValido: true,
        col: "",
        colValido: true,
        city: "",
        cityValido: true,
        state: "",
        stateValido: true,
        card: "",
        cardValido: true,
        dateExp: "",
        dateExpValido: true,
        ccv: "",
        ccvValido: true,
        totalAccount: 0
    });

    const navigate = useNavigate();

    const getEntities = () => {
        let productsOnCart = products.filter(product => product.isOnCart === true);
        let totalAccount = 0;
        productsOnCart.map((product:IProductModel) => {
            totalAccount += product.price;
            return
        })
        setState({
            ...state,
            productList: productsOnCart,
            totalAccount: totalAccount
        })
    }

    useEffect(() => {
        getEntities();
    }, [])

    useEffect(() => {

    }, [state.productList])

    const updateTotalAccount = (price: number) => {
        setState({
            ...state,
            totalAccount: state.totalAccount + price
        })
    }

    const removeProduct = (product: IProductModel) => {
        console.log("ROMEVE_FROM_CART");
        let index = products.findIndex(productFind => productFind.id === product.id);
        product.isOnCart = false;
        products[index] = product;
        getEntities();
    }

    const onChange = (field: string, value: string) => {
        switch(field){
            case "name":
                setState({
                    ...state,
                    name: value,
                    nameValid: value !== ""
                })
                break;
            case "street":
                setState({
                    ...state,
                    street: value,
                    streetValido: value !== ""
                })
                break;
            case "numExt":
                setState({
                    ...state,
                    numExt: value,
                    numExtValido: value !== ""
                })
                break;
            case "numInt":
                setState({
                    ...state,
                    numInt: value,
                })
                break;
            case "zip":
                setState({
                    ...state,
                    zip: value,
                    zipValido: value !== ""
                })
                break;
            case "col":
                setState({
                    ...state,
                    col: value,
                    colValido: value !== ""
                })
                break;
            case "city":
                setState({
                    ...state,
                    city: value,
                    cityValido: value !== ""
                })
                break;
            case "state":
                setState({
                    ...state,
                    state: value,
                    stateValido: value !== ""
                })
                break;
            case "card":
                setState({
                    ...state,
                    card: value,
                    cardValido: value !== ""
                })
                break;
            case "dateExp":
                setState({
                    ...state,
                    dateExp: value,
                    dateExpValido: value !== ""
                })
                break;
            case "ccv":
                setState({
                    ...state,
                    ccv: value,
                    ccvValido: value !== ""
                })
                break;
            default:
                break;
        }
    }

    const validateFields = () => {
        const { 
            name, 
            street, 
            numExt, 
            zip, 
            col, 
            city, 
            card, 
            dateExp, 
            ccv 
        } = state;
        let { 
            nameValid, 
            streetValido, 
            numExtValido, 
            zipValido, 
            colValido, 
            cityValido, 
            stateValido,
            cardValido, 
            dateExpValido, 
            ccvValido 
        } = state;
        nameValid = name !== "";
        streetValido = street !== "";
        numExtValido = numExt !== "";
        zipValido = zip !== "";
        colValido = col !== "";
        cityValido = city !== "";
        stateValido = state.state !== "";
        cardValido = card !== "";
        dateExpValido = dateExp !== "";
        ccvValido = ccv !== "";
        if (
            nameValid &&
            streetValido &&
            numExtValido &&
            zipValido &&
            colValido &&
            cityValido &&
            stateValido &&
            cardValido &&
            dateExpValido &&
            ccvValido 
        ){
            navigate("/payment-complete")
        }
        setState({
            ...state,
            nameValid, 
            streetValido, 
            numExtValido, 
            zipValido, 
            colValido, 
            cityValido, 
            stateValido,
            cardValido, 
            dateExpValido, 
            ccvValido 
        })
    }

    return(
        <Segment>
            <Grid className='address-container' >
                <Grid.Row textAlign='center' columns={1}>
                    <Grid.Column>
                        <Header>
                            <Header.Content>
                                Dirrección de entrega
                            </Header.Content>
                        </Header>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={2} textAlign='center' as={Segment}>
                    <Grid.Column>
                        <Input
                            className='payment-field'
                            label="Nombre *"
                            value={state.name}
                            onChange={(e) => onChange("name", e.currentTarget.value)}
                            error={!state.nameValid}
                        />
                    </Grid.Column>
                    <Grid.Column>
                        <Input
                            className='payment-field'
                            label="Calle *"
                            value={state.street}
                            onChange={(e) => onChange("street", e.currentTarget.value)}
                            error={!state.streetValido}
                        />
                    </Grid.Column>
                    <Grid.Column>
                        <Input
                            className='payment-field'
                            label="Núm Ext *"
                            value={state.numExt}
                            onChange={(e) => onChange("numExt", e.currentTarget.value)}
                            error={!state.numExtValido}
                        />
                    </Grid.Column>
                    <Grid.Column>
                        <Input
                            className='payment-field'
                            label="Núm Int"
                            value={state.numInt}
                            onChange={(e) => onChange("numInt", e.currentTarget.value)}
                        />
                    </Grid.Column>
                    <Grid.Column>
                        <Input
                            className='payment-field'
                            label="Colonia *"
                            value={state.col}
                            onChange={(e) => onChange("col", e.currentTarget.value)}
                            error={!state.colValido}
                        />
                    </Grid.Column>
                    <Grid.Column>
                        <Input
                            className='payment-field'
                            label="C.P. *"
                            value={state.zip}
                            onChange={(e) => onChange("zip", e.currentTarget.value)}
                            error={!state.zipValido}
                        />
                    </Grid.Column>
                    <Grid.Column>
                        <Input
                            className='payment-field'
                            label="Ciudad *"
                            value={state.city}
                            onChange={(e) => onChange("city", e.currentTarget.value)}
                            error={!state.cityValido}
                        />
                    </Grid.Column>
                    <Grid.Column>
                        <Input
                            className='payment-field'
                            label="Estado *"
                            value={state.state}
                            onChange={(e) => onChange("state", e.currentTarget.value)}
                            error={!state.stateValido}
                        />
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row textAlign='center' columns={1}>
                    <Grid.Column>
                        <Header>
                            <Header.Content>
                                Forma de pago
                            </Header.Content>
                        </Header>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row as={Segment} columns={3} textAlign="center">
                    <Grid.Column computer={8}>
                        <Input
                            className='payment-field'
                            label="Tarjeta *"
                            value={state.card}
                            onChange={(e) => onChange("card", e.currentTarget.value)}
                            error={!state.cardValido}
                        />
                    </Grid.Column>
                    <Grid.Column computer={4}>
                        <Input
                            className='payment-method'
                            label="Fecha expiración *"
                            value={state.dateExp}
                            onChange={(e) => onChange("dateExp", e.currentTarget.value)}
                            error={!state.dateExpValido}
                        />
                    </Grid.Column>
                    <Grid.Column computer={4}>
                        <Input
                            className='payment-method'
                            label="CCV *"
                            value={state.ccv}
                            onChange={(e) => onChange("ccv", e.currentTarget.value)}
                            error={!state.ccvValido}
                        />
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row textAlign='center' columns={1}>
                    <Grid.Column>
                        <Header>
                            <Header.Content>
                                Pedido
                            </Header.Content>
                        </Header>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row as={Segment} columns={1} textAlign="center" style={{ marginBottom: '40px' }}>
                    <Grid.Column>
                        <Grid className="product-container">
                            {state.productList.map((product: IProductModel) => {
                                
                                return(
                                    <Grid.Row key={`cart-${product.id}`} columns={4}>
                                        <Grid.Column>
                                            <Image src={`assets/products/${product.image}`} size="mini"/>
                                        </Grid.Column>
                                        <Grid.Column>
                                            <h3>{product.title}</h3>
                                        </Grid.Column>
                                        <Grid.Column>
                                            <h3>{`$${product.price.toFixed(2)}`}</h3>
                                        </Grid.Column>
                                        <Grid.Column>
                                            <Button
                                                circular 
                                                icon="trash"
                                                color='red'
                                                onClick={() => removeProduct(product)}
                                            />
                                        </Grid.Column>
                                    </Grid.Row>
                                );
                            })}
                            <Grid.Row columns={4}>
                                <Grid.Column>

                                </Grid.Column>
                                <Grid.Column>
                                    <h3>Total:</h3>
                                </Grid.Column>
                                <Grid.Column>
                                    <h3>{`$${state.totalAccount.toFixed(2)}`}</h3>
                                </Grid.Column>
                                <Grid.Column>
                                    
                                </Grid.Column>
                            </Grid.Row>
                            {state.productList.length > 0 ? 
                            <Grid.Row textAlign='center'>
                                <Grid.Column>
                                    <Button 
                                        color='blue' 
                                        onClick={validateFields}
                                    >
                                        Pagar
                                    </Button>
                                </Grid.Column>
                            </Grid.Row>
                            :
                            <Grid.Row textAlign='center'>
                                <Grid.Column>
                                    <Button 
                                        color='blue' 
                                        onClick={() => navigate("/")}
                                    >
                                        Agregar productos
                                    </Button>
                                </Grid.Column>
                            </Grid.Row>
                            }
                        </Grid>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>
    );
}

export default Payment;