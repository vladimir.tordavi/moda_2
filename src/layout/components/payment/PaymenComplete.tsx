import * as React from 'react';
import { Header, Icon, Segment } from 'semantic-ui-react';
import "../../../css/payment.css";

const PaymentComplete = () => {
    return(
        <Segment className='payment-complete-container'>
            <Header className='payment-complete-message' textAlign='center'>
                <Header.Content className='payment-complete-header'>
                    <Icon name='check' size='massive' color='green'/><br/>
                    PAGO COMPLETADO CON ÉXITO!
                </Header.Content>
                <Header.Subheader>
                    Tu pedido se está preparando, pronto recibirás un correo con tu guía de seguimiento
                </Header.Subheader>
            </Header>
        </Segment>
    );
}

export default PaymentComplete;