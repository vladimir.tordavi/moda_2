import * as React from 'react';
import { Segment, Grid, Header } from 'semantic-ui-react';
import Categories from './components/Categories';
import Products from './components/Products';

const Store = () => {
    return (
        <Segment>
            <Grid>
                <Grid.Row columns={2}>
                    <Grid.Column computer={4}>
                        <Header textAlign='center'>Categorías</Header>
                    </Grid.Column>
                    <Grid.Column computer={8}>
                        <Header textAlign='center'>Productos</Header>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={2}>
                    <Grid.Column computer={4}>
                        <Categories/>
                    </Grid.Column>
                    <Grid.Column computer={12}>
                        <Products/>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Segment>
    );
}

export default Store;