import React, { useState } from 'react'
import { Grid, Image } from 'semantic-ui-react';
import '../../../../css/gallery.product.css';

interface IGalleryProps {
    gallery: any[]
}

interface IGalleryState {
    selectedIndex: number
}

const Gallery = (props: IGalleryProps) => {
    const { gallery } = props;

    const [state, setState] = useState<IGalleryState>({
        selectedIndex: 0
    })
    
    return(
        <div>
            <Grid>
                {gallery ? 
                <Grid.Row columns={2}>
                    <Grid.Column computer={2} mobile={16}>
                        {gallery.map((image, value) => {
                            return(
                                <Image
                                    className="image-dot"
                                    src={`/assets/products/${image}`}
                                    onMouseEnter={() => {
                                        setState({ ...state, selectedIndex: value })
                                    }}
                                    onClick={() => {
                                        setState({ ...state, selectedIndex: value })
                                    }}
                                />
                            );
                        })}
                    </Grid.Column>
                    <Grid.Column computer={14} mobile={16}>
                        <Image
                            className="image-selected"
                            src={
                                `/assets/products/${gallery[state.selectedIndex]}`
                            }
                        />
                    </Grid.Column>
                </Grid.Row>
                : null}
            </Grid>
        </div>
    );
}

export default Gallery;