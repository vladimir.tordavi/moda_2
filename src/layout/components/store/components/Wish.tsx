import React, { useState, useEffect } from 'react';
import { Popup, Icon, Grid, Image, Button } from 'semantic-ui-react';
import IProductModel from '../../../../models/product.model';
import products from '../../../../data/products.data';
import "../../../../css/cart.css";
import { useNavigate } from 'react-router-dom';

interface IState {
    open: boolean;
    productsList: IProductModel[]
}

const Wish = () => {

    const navigate = useNavigate();

    const [state, setState] = useState<IState>({
        open: false,
        productsList: []
    });

    const getEntities = () => {
        console.log("PRODUCTS", products);
        let productsOnCart = products.filter(product => product.isOnWishes === true);
        setState({
            ...state,
            productsList: productsOnCart
        })
    }

    useEffect(() => {
        getEntities();
    }, [])

    useEffect(() => {
        getEntities();
    }, [products])

    useEffect(() => {
        getEntities();
    }, [state.open])

    const removeProduct = (product: IProductModel) => {
        console.log("REMOVE_FAVORITES");
        let index = products.findIndex(productFind => productFind.id === product.id);
        product.isOnWishes = false;
        products[index] = product;
        getEntities();
    }

    return (
        <Popup
            position='bottom center'
            hoverable
            closeOnEscape
            closeOnDocumentClick
            closeOnTriggerMouseLeave={false}
            onClose={() => setState({ ...state, open: false })}
            className="popup-container"
            trigger={
                <Icon 
                    className="icon-item pointer"
                    size="large"
                    name='heart'
                    onClick={() => {
                        setState({
                            ...state,
                            open: !state.open
                        })
                    }}
                />
            }
            open={state.open}
        >
            <Grid style={{ width: '300px'}}>
                {state.productsList.map((product: IProductModel) => {
                    return(
                        <Grid.Row key={`cart-${product.id}`} columns={3}>
                            <Grid.Column>
                                <Image src={`/assets/products/${product.image}`} size="mini"/>
                            </Grid.Column>
                            <Grid.Column>
                                <h6>{product.title}</h6>
                            </Grid.Column>
                            <Grid.Column>
                                <Button
                                    circular 
                                    icon="trash"
                                    color='red'
                                    onClick={() => removeProduct(product)}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    );
                })}
                {state.productsList.length === 0 ?
                <Grid.Row textAlign='center'>
                    <Grid.Column>
                        <Button 
                            color='blue' 
                            onClick={() => navigate("/")}
                        >
                            Ver Productos
                        </Button>
                    </Grid.Column>
                </Grid.Row>
                : null}
            </Grid>
        </Popup>
    );
}

export default Wish;