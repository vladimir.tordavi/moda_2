import React, { useEffect, useState } from 'react'
import { Button, Header, Grid, Segment, Icon, Image } from 'semantic-ui-react'
import { useParams } from 'react-router-dom';
import Gallery from './Gallery';
import '../../../../css/detail.product.css';
import IProductModel from '../../../../models/product.model';
import products from '../../../../data/products.data';

interface IState {
    product?: IProductModel
}

const DetailProduct = () => {
  const { id } = useParams();
  const [state, setState] = useState<IState>({
  })

  useEffect(() => {
    if(id) {
        getEntity();
    }
  }, [])

  useEffect(() => {
    if(id) {
        getEntity();
    }
  }, [id])

  const getEntity = () => {
    let product = products.filter(product => product.id === id)[0];
    setState({
        ...state,
        product: product
    })
  }

  const addToCart = (product: IProductModel) => {
    console.log("ADD_TO_CART");
    let index = products.findIndex(productFind => productFind.id === product.id);
    product.isOnCart = true;
    products[index] = product;
  }

  const addToFavorites = (product: IProductModel) => {
    console.log("ADD_TO_FAVORITES");
    let index = products.findIndex(productFind => productFind.id === product.id);
    product.isOnWishes = true;
    products[index] = product;
  }

  const { product } = state;

  return (
    <Segment className="wd-80 center-frame"> 
        {product ?
          <Grid>
            <Grid.Row>
              <Grid.Column>
                <Header textAlign="left">
                  <Header.Content >
                    {product.id ? `SKU: ${product.id?.toUpperCase()}` : ""}
                  </Header.Content>
                </Header>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={2}>
              <Grid.Column computer={12} mobile={16} className="p40">
                <Gallery gallery={product.image ? [product.image] : []}/>
              </Grid.Column>
              <Grid.Column computer={4} mobile={16}>
                <div className="description-container">
                  <h2 className="title-product">
                    {product.title ? product.title.toUpperCase() : ""}
                  </h2>
                  <h5 className="price-product title-product">
                    {`$${product.price ? product.price.toFixed(2) : 0}`}
                  </h5>
                  <h5 className="description-product">
                    Descripción: {product.description}
                  </h5>
                  <h5 className="description-product">
                    Marca: {product.brand}
                  </h5>
                  <h5 className="description-product">
                    Material: {product.material}
                  </h5>
                  <h5 className="send-text green">
                    <Icon name="box"/>
                    Envío en 2 horas!
                  </h5>
                  <Button
                    className="description-button w80"
                    color="red"
                    style={{
                      marginTop: '20px'
                    }}
                    onClick={() => addToFavorites(product)}
                  >
                    Agregar a favoritos
                  </Button>
                  <Button
                    className="description-button w80"
                    color="blue"
                    style={{
                      marginTop: '20px'
                    }}
                    onClick={() => addToCart(product)}
                  >
                    Agregar al carrito
                  </Button>
                  <h5 className="text-protection">
                    <Icon name="shield"/>
                    Compra Protegida<br/>
                    Recibe el producto que esperabas o te devolvemos tu dinero.
                  </h5>
                </div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        : <h1>PRODUCTO NO ENCONTRADO</h1>}
    </Segment>
  )
}

export default DetailProduct;