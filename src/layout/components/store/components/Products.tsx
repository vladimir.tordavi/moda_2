import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Grid } from 'semantic-ui-react';
import products from '../../../../data/products.data';
import IProductModel from '../../../../models/product.model';
import ProductComponent from './Product.component';

interface IState {
    productList: IProductModel[]
}

const Products = () => {
    const { category } = useParams();

    const [state, setState] = useState<IState>({
        productList: []
    })

    useEffect(() => {
        getEntities();
      }, [])
    
      useEffect(() => {
        getEntities();
      }, [category])

    const getEntities = () => {
        if(category && category !== "TODO") {
            let filterProducts = products.filter(product => product.category === category);
            setState({
                ...state,
                productList: filterProducts
            })
        } else {
            setState({
                ...state,
                productList: products
            })
        }
    }

    return (
        <div>
            <Grid>
                <Grid.Row>
                {state.productList.length>0 ? state.productList.map((product: IProductModel) => {
                    return (
                        <Grid.Column computer={4} tablet={8} mobile={16} key={product.id}>
                            <ProductComponent product={product}/>
                        </Grid.Column>
                    );
                }): 
                <Grid.Column textAlign='center'>
                    <h2>No hay productos disponibles</h2>    
                </Grid.Column>}
                </Grid.Row>
            </Grid>
        </div>
    );
}

export default Products;