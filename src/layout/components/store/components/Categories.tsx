import * as React from 'react';
import { Grid } from 'semantic-ui-react';
import categories from '../../../../data/categories.data';
import ICategoryModel from '../../../../models/category.model';
import '../../../../css/categories.css';
import { useNavigate } from 'react-router-dom';

const Categories = () => {
    
    const navigate = useNavigate();

    return (
        <div>
            <Grid style={{ width: '50%', marginLeft: "auto", marginRight: "auto" }}>
                {categories.map((category: ICategoryModel) => {
                    return (
                        <Grid.Row id={category.id}>
                            <Grid.Column>
                                <h6 
                                    className='category-title pointer'
                                    onClick={() => navigate(`/${category.title}`)}
                                >
                                    {category.title}
                                </h6>
                            </Grid.Column>
                        </Grid.Row>
                    );
                })}
            </Grid>
        </div>
    );
}

export default Categories;