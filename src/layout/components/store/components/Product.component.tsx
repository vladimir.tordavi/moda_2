import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import { Card, Image, Icon, Button } from 'semantic-ui-react';
import IProductModel from '../../../../models/product.model';

interface IProps {
    product: IProductModel;
}
const ProductComponent = (props: IProps) => {
    const { product } = props;
    const navigate = useNavigate();
    
    return(
        <Card onClick={() => navigate(`/product/${product.id}`)}>
            <Image src={`/assets/products/${product.image}`} wrapped ui={false} />
            <Card.Content>
                <Card.Header>{product.title}</Card.Header>
                <Card.Meta>
                    <span className='date'>{product.brand}</span>
                </Card.Meta>
                <Card.Description>
                    {product.description}
                </Card.Description>
            </Card.Content>
            <Card.Content extra textAlign='center'>
                <Button color='blue'>Ver Detalle</Button>
            </Card.Content>
        </Card>
    );
}

export default ProductComponent;