import React, { useState, useEffect } from 'react';
import { Popup, Icon, Grid, Image, Button } from 'semantic-ui-react';
import IProductModel from '../../../../models/product.model';
import products from '../../../../data/products.data';
import "../../../../css/cart.css";
import { useNavigate } from 'react-router-dom';

interface IState {
    open: boolean;
    productsList: IProductModel[]
}

const Cart = () => {

    const navigate = useNavigate();

    const [state, setState] = useState<IState>({
        open: false,
        productsList: []
    });

    const getEntities = () => {
        let productsOnCart = products.filter(product => product.isOnCart === true);
        setState({
            ...state,
            productsList: productsOnCart
        })
    }

    useEffect(() => {
        getEntities();
    }, [])

    useEffect(() => {
        getEntities();
    }, [state.open])

    useEffect(() => {
        console.log("UPDATE CART")
        getEntities();
    }, [products])

    const removeProduct = (product: IProductModel) => {
        console.log("ROMEVE_FROM_CART");
        let index = products.findIndex(productFind => productFind.id === product.id);
        product.isOnCart = false;
        products[index] = product;
        getEntities();
    }

    return (
        <Popup
            position='bottom center'
            hoverable
            closeOnEscape
            closeOnDocumentClick
            closeOnTriggerMouseLeave={false}
            onClose={() => setState({ ...state, open: false })}
            className="popup-container"
            trigger={
                <Icon 
                    className="icon-item pointer"
                    size="large"
                    name='cart'
                    onClick={() => {
                        setState({
                            ...state,
                            open: !state.open
                        })
                    }}
                />
            }
            open={state.open}
        >
            <Grid style={{ width: '300px'}}>
                {state.productsList.map((product: IProductModel) => {
                    return(
                        <Grid.Row key={`cart-${product.id}`} columns={3}>
                            <Grid.Column>
                                <Image src={`/assets/products/${product.image}`} size="mini"/>
                            </Grid.Column>
                            <Grid.Column>
                                <h6>{product.title}</h6>
                            </Grid.Column>
                            <Grid.Column>
                                <Button
                                    circular 
                                    icon="trash"
                                    color='red'
                                    onClick={() => removeProduct(product)}
                                />
                            </Grid.Column>
                        </Grid.Row>
                    );
                })}
                {state.productsList.length > 0 ? 
                    <Grid.Row textAlign='center'>
                        <Grid.Column>
                            <Button 
                                color='blue' 
                                onClick={() => navigate("/payment")}
                            >
                                Ir a pagar
                            </Button>
                        </Grid.Column>
                    </Grid.Row>
                :
                <Grid.Row textAlign='center'>
                    <Grid.Column>
                        <Button 
                            color='blue' 
                            onClick={() => navigate("/")}
                        >
                            Ver Productos
                        </Button>
                    </Grid.Column>
                </Grid.Row>
                }
            </Grid>
        </Popup>
    );
}

export default Cart;