import React, { useState } from 'react';
import { Grid, Image, Segment, Input, Button } from 'semantic-ui-react';
import logo from '../../../logo.svg';
import '../../../css/login.css';
import { useNavigate } from 'react-router-dom';

interface IState {
    user: string;
    userValid: boolean;
    password: string;
    passwordValid: boolean;
    errorMessage: string;
}

const Login = () => {

    const navigate = useNavigate();

    const [state, setState] = useState<IState>({
        user: "",
        userValid: true,
        password: "",
        passwordValid: true,
        errorMessage: ""
    })

    const login = () => {
        let { userValid, passwordValid } = state;
        userValid = state.user !== "";
        passwordValid = state.password !== "";
        let errorMessage = "";
        if(state.user !== "admin") {
            errorMessage = "Usuario incorrecto";
        } else {
            if(state.password !== "admin") {
                errorMessage = "Contraseña incorrecta";
            } else {
                navigate("/");
            }
        }
        setState({
            ...state,
            userValid,
            passwordValid,
            errorMessage: errorMessage
        })
    }

    return (
        <div className='login-container'>
            <Segment className='login-box'>
                <Grid>
                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <Image 
                                src={logo} 
                                size="medium"
                                centered
                            />
                        </Grid.Column>
                        <Grid.Column>
                            <Input 
                                className='input-field'
                                type="email"
                                label="Usuario *"
                                value={state.user}
                                onChange={(e) => {
                                    setState({
                                        ...state,
                                        user: e.currentTarget.value,
                                        userValid: e.currentTarget.value !== ""
                                    })
                                }}
                                error={!state.userValid}
                            />
                        </Grid.Column>
                        <Grid.Column>
                            <Input 
                                className='input-field'
                                type="password"
                                label="Contraseña *"
                                value={state.password}
                                onChange={(e) => {
                                    setState({
                                        ...state,
                                        password: e.currentTarget.value,
                                        passwordValid: e.currentTarget.value !== ""
                                    })
                                }}
                                error={!state.passwordValid}
                            />
                        </Grid.Column>
                        {!state.passwordValid || !state.userValid ? <Grid.Column textAlign='center'> 
                            <h4 style={{ color: 'red', marginBottom: "40px" }}>Campos con * son requeridos</h4>
                        </Grid.Column> : null}
                        {state.errorMessage !== "" ? <Grid.Column textAlign='center'> 
                            <h4 style={{ color: 'red', marginBottom: "40px" }}>{state.errorMessage}</h4>
                        </Grid.Column> : null}
                        <Grid.Column textAlign='center'>
                            <Button 
                                color='blue'
                                onClick={login}
                            >
                                Iniciar Sesión
                            </Button>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        </div>
    );
}

export default Login;