import React from 'react';
import { Grid } from 'semantic-ui-react';
import logo from '../logo.svg';
import '../css/footer.css';
import { useNavigate } from 'react-router-dom';

const Footer = () => {
    const navigate = useNavigate();
    return(
        <div className='footer-container'>
            <Grid>
                <Grid.Row textAlign='center'>
                    <Grid.Column>
                        <img 
                            src={logo} 
                            className="App-logo pointer" 
                            alt="logo" 
                            onClick={() => navigate("/")}    
                        />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </div>
    );
}

export default Footer;