import React from 'react'
import {
    Routes as Switch,
    Route,
} from "react-router-dom";
import Login from './components/login/Login';
import PaymentComplete from './components/payment/PaymenComplete';
import Payment from './components/payment/Payment';
import DetailProduct from './components/store/components/ProductDetail';
import Store from './components/store/Store';

const Routes = () => {

    return(
            <Switch>
                <Route
                    path='/'
                    element={<Store/>}
                />
                <Route
                    path='/:category'
                    element={<Store/>}
                />
                <Route
                    path='/product/:id'
                    element={<DetailProduct/>}
                />
                <Route
                    path='/login'
                    element={<Login/>}
                />
                <Route
                    path='/payment'
                    element={<Payment/>}
                />
                <Route
                    path='/payment-complete'
                    element={<PaymentComplete/>}
                />
            </Switch>
    );
}

export default Routes;
