import React from 'react';
import Routes from './Routes';
import '../css/body.css';

const Body = () => {

    return(
        <div className='body-container'>
            <Routes/>
        </div>
    );
}

export default Body;