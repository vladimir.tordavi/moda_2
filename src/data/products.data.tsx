import IProductModel from "../models/product.model";
import * as sizez from '../constants/sizes.constants';
import * as categories from '../constants/categories.contants';

const Products: IProductModel[] = [
    {
        id: "prod-01",
        title: "Sudadera",
        description: "Sudadera negra con blanco",
        material: "100% algodón",
        brand: "Pink Floyd",
        size: sizez.SIZE_G,
        colors: [
            "white", 
            "pink",
            "black",
            "blue"
        ],
        inventary: 5,
        price: 130,
        category: categories.SUDADERAS,
        isOnCart: true,
        isOnWishes: false,
        image: "sudadera.png"
    },
    {
        id: "prod-02",
        title: "Pantalón",
        description: "Sudadera negra con blanco",
        material: "100% algodón",
        brand: "Pink Floyd",
        size: sizez.SIZE_G,
        colors: [
            "white", 
            "pink",
            "black",
            "blue"
        ],
        inventary: 5,
        price: 130,
        category: categories.SUDADERAS,
        isOnCart: false,
        isOnWishes: true,
        image: ""
    },
    {
        id: "prod-03",
        title: "Abrigo",
        description: "Sudadera negra con blanco",
        material: "100% algodón",
        brand: "Pink Floyd",
        size: sizez.SIZE_G,
        colors: [
            "white", 
            "pink",
            "black",
            "blue"
        ],
        inventary: 5,
        price: 130,
        category: categories.SUDADERAS,
        isOnCart: false,
        isOnWishes: false,
        image: ""
    }

];

export default Products;