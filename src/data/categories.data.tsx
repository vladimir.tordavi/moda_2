import ICategoryModel from "../models/category.model";
import * as cat from '../constants/categories.contants';

const categories: ICategoryModel[] = [
    {
        id: "cat-01",
        title: cat.ABRIGOS,
        icon: ""
    },
    {
        id: "cat-02",
        title: cat.BLUSAS,
        icon: ""
    },
    {
        id: "cat-03",
        title: cat.FALDAS,
        icon: ""
    },
    {
        id: "cat-04",
        title: cat.PANTALONES,
        icon: ""
    },
    {
        id: "cat-05",
        title: cat.SUDADERAS,
        icon: ""
    },
    {
        id: "cat-06",
        title: cat.TODO,
        icon: ""
    },
];

export default categories;